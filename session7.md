# Research seminar on probabilistic data analysis: Session 5 (May 15, 2020)

  * [Zoom link](zoom.md)
  * [Zulip chat](https://utu.zulipchat.com/#narrow/stream/230589-bda)



# Discussion topic 1 (Joonatan Palmu): Chapter 9; decision analysis

    * Main reading: 9.1, 9.3, 9.5
    * I would ask everyone to think a moment
        - One idea on your field where decision analysis might be useful
        - Should the decision analysis be avoided in some particular situation
    * Optional reading: 9.2, 9.4, trialr package for R

# Discussion topic 2 (Rohit Goswami): Chapter 11; approximations and sampling

## Main reading

Sections 11.1, 11.2, 11.4, 11.6

## Optional Reading

- Convergence Diagnostics for Markov Chain Monte Carlo (see GitLab or [arxiv](https://arxiv.org/abs/1909.11827))
  - Section 2
- [Markov Chain Monte Carlo in Practice](https://betanalpha.github.io/assets/case_studies/markov_chain_monte_carlo.html)
- Practical Computations beyond the Metropolis-Hastings and Gibbs (the Stan view)
  - [No-U-turn Sampler](http://www.stat.columbia.edu/~gelman/research/published/nuts.pdf)
  - [Hamiltonian Monte Carlo](https://mc-stan.org/docs/2_23/reference-manual/hamiltonian-monte-carlo.html)

## Thoughts

- Can meaningful inferences be drawn from non-convergence?
