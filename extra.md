# Extra material

Below some ideas for later extra sessions.

## Case studies

  * [Betancourt's page](https://betanalpha.github.io/writing/)

## Session: Stan

[Introductory article in Journal of Statistical Software](http://www.stat.columbia.edu/~gelman/research/published/Stan-paper-aug-2015.pdf)

[Betancourt's Stan Intro](https://github.com/betanalpha/stan_intro)

Basics of Bayesian inference and Stan, Jonah Gabry & Lauren Kennedy:
  * [Part 1](https://www.youtube.com/watch?v=ZRpo41l02KQ&t=8s&list=PLuwyh42iHquU4hUBQs20hkBsKSMrp6H0J&index=6)
  * [Part 2](https://www.youtube.com/watch?v=6cc4N1vT8pk&t=0s&list=PLuwyh42iHquU4hUBQs20hkBsKSMrp6H0J&index=7)

[Stan documentation](Introductory article in Journal of Statistical Software)


## Session: Model selection

 * [Model selection tutorial](https://github.com/avehtari/modelselection) by Aki Vehtari

