data("windshieldy1")
data("windshieldy2")

## Functions **************** ####
# Likelihood
likelihood <- function(pars, data) { # pars = c(mu, sigma)
  prod(
    dnorm(data, pars[1], pars[2]) 
  )
}

# Uninformative prior
prior <- function(pars) { 
  pars[2]^(-2)
}

# Unnormalized posterior
posterior <- function(pars, data) {
  
  likelihood(pars, data) * prior(pars)
  
}


## Grid ********************* ####

# Set grid for plotting
mu_grid <- seq(12, 17, length.out = 100)
sigma_grid <- seq(0.5, 3, length.out = 100)


# Grid sizes for normalization
mu_step <- mu_grid[2] - mu_grid[1]
sigma_step <- sigma_grid[2] - sigma_grid[1]

cell_area <- mu_step*sigma_step




## Posterior **************** ####

# Initialize posterior df
posterior_df <- expand.grid(mu_grid, sigma_grid) %>% 
  set_colnames(c("mu", "sigma"))


for(i in 1:nrow(posterior_df)) {
  
  pars <- as.numeric(posterior_df[i, c("mu", "sigma")])
  
  posterior_df[i, "likelihood1"] <- likelihood(pars, windshieldy1)
  posterior_df[i, "likelihood2"] <- likelihood(pars, windshieldy2)
  
  posterior_df[i, "posterior1"] <- posterior(pars, windshieldy1)
  
  posterior_df[i, "posterior2"] <- posterior(pars, windshieldy2)
}

# Normalize
for(distribution in c("likelihood1", "likelihood2",
                      "posterior1", "posterior2")) {
  
  posterior_df[, distribution] <- posterior_df[, distribution]/(sum(posterior_df[, distribution])*cell_area)
  
}
## Plot posterior

# Plot posterior
p_posterior <- posterior_df %>% 
  melt(c("sigma", "mu")) %>% 
  ggplot(aes(x = mu, y = sigma, z = value, color = variable)) +
  geom_contour()
  

p_posterior



## Posterior difference ***** ####

# Sample t distribution
rtnew <- function(n, df, mean = 0, scale = 1, ...) {
  (stats::rt(n, df = df)*scale + mean)*scale
}


# Sample posteriors and compute difference
sample_df <- data.frame(
  samples1 = rtnew(1000, 
                   df = length(windshieldy1) - 1, 
                   mean = mean(windshieldy1),
                   scale = sqrt(1 + 1/length(windshieldy1))), 
  samples2 = rtnew(1000, 
                   df = length(windshieldy2) - 1, 
                   mean = mean(windshieldy2),
                   scale = sqrt(1 + 1/length(windshieldy2))))

# Difference
sample_df <- sample_df %>% 
  mutate(delta_mu = samples2 - samples1)




## Plot ********************* ####

p_difference_posterior <- sample_df %>% 
  ggplot(aes(x = delta_mu)) + 
  geom_histogram(bins = 30, color = "black", fill = "steelblue") +
  geom_vline(xintercept = mean(sample_df$delta_mu), color = "red") +
  geom_vline(xintercept = quantile(sample_df$delta_mu, c(.025)), color = "black") +
  geom_vline(xintercept = quantile(sample_df$delta_mu, c(.975)), color = "black") +
  labs(title = "Difference posterior", 
       subtitle = paste0("Mean = ",
                         mean(sample_df$delta_mu) %>%
                           round(2), 
                         ", 95% interval = [",
                         quantile(sample_df$delta_mu, c(.025)) %>%
                           round(2), 
                         ", ", 
                         quantile(sample_df$delta_mu, c(.975)) %>%
                           round(2), 
                         "]"))


p_difference_posterior


# Given the uninformative priors
# there is evidence for that the line 2
# produces stronger windshieds with probability mean(sample_df$delta_mu >= 0) = 0.758


## b) *********************** ####

# Probability = 0